/**
 ****************************************************************************************
 *
 * @file proxr_task.c
 *
 * @brief Proximity Reporter Task implementation.
 *
 * Copyright (C) RivieraWaves 2009-2012
 *
 * $Rev: $
 *
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @addtogroup PROXRTASK
 * @{
 ****************************************************************************************
 */


/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "app_config.h"

#if (BLE_PROX_REPORTER)

#include "gap.h"
#include "gatt_task.h"
#include "atts_util.h"
#include "proxr.h"
#include "proxr_task.h"
#include "attm_cfg.h"
#include "usr_design.h"
#include "pwm.h"

/*
 * FUNCTION DEFINITIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Handles reception of the @ref PROXR_CREATE_DB_REQ message.
 * The handler adds LLS and optionally TXPS into the database.
 * @param[in] msgid Id of the message received (probably unused).
 * @param[in] param Pointer to the parameters of the message.
 * @param[in] dest_id ID of the receiving task instance (probably unused).
 * @param[in] src_id ID of the sending task instance.
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
static int proxr_create_db_req_handler(ke_msg_id_t const msgid,
                                       struct proxr_create_db_req const *param,
                                       ke_task_id_t const dest_id,
                                       ke_task_id_t const src_id)
{
    //Database Creation Status
    uint8_t status;

    //Save Profile ID
    proxr_env.con_info.prf_id = TASK_PROXR;
					
    //Add Service Into Database
    status = atts_svc_create_db_ext(&proxr_env.ibc_shdl, NULL, IDX_NB, NULL, dest_id, &proxr_att_db[0]);
		
    //Disable LLS
    attsdb_svc_set_permission(proxr_env.ibc_shdl, PERM(SVC, DISABLE));

    //Go to Idle State
    if (status == ATT_ERR_NO_ERROR)
    {
        //If we are here, database has been fulfilled with success, go to idle state
        ke_state_set(TASK_PROXR, PROXR_IDLE);
    }
		
    return (KE_MSG_CONSUMED);
}

/**
 ****************************************************************************************
 * @brief Enable the Proximity Reporter role, used after connection.
 * @param[in] msgid     Id of the message received.
 * @param[in] param     Pointer to the parameters of the message.
 * @param[in] dest_id   ID of the receiving task instance
 * @param[in] src_id    ID of the sending task instance.
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
static int proxr_enable_req_handler(ke_msg_id_t const msgid, struct proxr_enable_req const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id)
{	
    // Keep source of message, to respond to it further on
    proxr_env.con_info.appid = src_id;
    // Store the connection handle for which this profile is enabled
    proxr_env.con_info.conhdl = param->conhdl;

    // Check if the provided connection exist
    if (gap_get_rec_idx(param->conhdl) == GAP_INVALID_CONIDX)
    {
        // The connection doesn't exist, request disallowed
        prf_server_error_ind_send((prf_env_struct *)&proxr_env, PRF_ERR_REQ_DISALLOWED, PROXR_ERROR_IND, PROXR_ENABLE_REQ);
    }
    else
    {
        // Enable IBC + Set Security Level
        attsdb_svc_set_permission(proxr_env.ibc_shdl, PERM(SVC, ENABLE));
				
				attsdb_att_set_value(proxr_env.ibc_shdl + IDX_BC_VAL, sizeof(uint8_t) * CHAR_1_LENGTH, buffer);			
				attsdb_att_set_value(proxr_env.ibc_shdl + IDX_MM_VAL, sizeof(uint8_t) * CHAR_2_LENGTH, &buffer[CHAR_1_LENGTH]);
				attsdb_att_set_value(proxr_env.ibc_shdl + IDX_BT_VAL, sizeof(uint8_t) * CHAR_3_LENGTH, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH]);
				attsdb_att_set_value(proxr_env.ibc_shdl + IDX_LD_VAL, sizeof(uint8_t) * CHAR_4_LENGTH, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH]);
	
				attsdb_att_set_value(proxr_env.ibc_shdl + IDX_NM_VAL, sizeof(uint8_t) * CHAR_5_LENGTH, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH + CHAR_4_LENGTH]);
				attsdb_att_set_value(proxr_env.ibc_shdl + IDX_OG_VAL, sizeof(uint8_t) * CHAR_6_LENGTH, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH + CHAR_4_LENGTH + CHAR_5_LENGTH]);
			 
        // Go to Connected state
        ke_state_set(TASK_PROXR, PROXR_CONNECTED);
    }

    return (KE_MSG_CONSUMED);
}

/**
 ****************************************************************************************
 * @brief Handles reception of the @ref GATT_WRITE_CMD_IND message.
 * The handler will analyse what has been set and decide alert level
 * @param[in] msgid Id of the message received (probably unused).
 * @param[in] param Pointer to the parameters of the message.
 * @param[in] dest_id ID of the receiving task instance (probably unused).
 * @param[in] src_id ID of the sending task instance.
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
static int gatt_write_cmd_ind_handler(ke_msg_id_t const msgid, struct gatt_write_cmd_ind const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    uint8_t char_code = 0;
    uint8_t status = PRF_APP_ERROR;
		uint32_t reg_mask0 = P16_MASK_PIN_CTRL | P17_MASK_PIN_CTRL, reg_mask1 = P26_MASK_PIN_CTRL;
		
    if (param->conhdl == proxr_env.con_info.conhdl)
    {
        if  (param->handle == proxr_env.ibc_shdl + IDX_BC_VAL){
						char_code = CHAR_1_LENGTH;
						memcpy(buffer, (uint8_t *)&param->value[0], char_code);
						register_flag = 1;
				}
			
				else if  (param->handle == proxr_env.ibc_shdl + IDX_LD_VAL){
						char_code = CHAR_4_LENGTH;
						memcpy(&buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH], (uint8_t *)&param->value[0], char_code);
					
						syscon_SetPMCR0WithMask(QN_SYSCON, reg_mask0, P15_PWM1_PIN_CTRL | P16_PWM0_PIN_CTRL);
						syscon_SetPMCR1WithMask(QN_SYSCON, reg_mask1, P26_GPIO_22_PIN_CTRL);
					
						if(param->value[0] == 0x01){
							pwm_config(PWM_CH1, PWM_PSCAL_DIV, PWM_COUNT_MS(20, PWM_PSCAL_DIV), PWM_COUNT_US(2000, PWM_PSCAL_DIV));
							pwm_config(PWM_CH0, PWM_PSCAL_DIV, PWM_COUNT_MS(20, PWM_PSCAL_DIV), PWM_COUNT_US(1000, PWM_PSCAL_DIV));
						}
						else if (param->value[0] == 0x00){
							pwm_config(PWM_CH1, PWM_PSCAL_DIV, PWM_COUNT_MS(20, PWM_PSCAL_DIV), PWM_COUNT_US(1000, PWM_PSCAL_DIV));
							pwm_config(PWM_CH0, PWM_PSCAL_DIV, PWM_COUNT_MS(20, PWM_PSCAL_DIV), PWM_COUNT_US(2000, PWM_PSCAL_DIV));
						}
						
						delay(160000);
				
						syscon_SetPMCR0WithMask(QN_SYSCON, reg_mask0, P15_GPIO_13_PIN_CTRL | P16_GPIO_14_PIN_CTRL);
				}
				
				else if(param->handle == proxr_env.ibc_shdl + IDX_NM_VAL){
						char_code = CHAR_5_LENGTH;
						memcpy(&buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH + CHAR_4_LENGTH], (uint8_t *)&param->value[0], char_code);
				}
				
				else if(param->handle == proxr_env.ibc_shdl + IDX_OG_VAL){
						char_code = CHAR_6_LENGTH;
						memcpy(&buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH + CHAR_4_LENGTH + CHAR_5_LENGTH], (uint8_t *)&param->value[0], char_code);
				}
        
        if (char_code){					
          //Save value in DB
					status = PRF_ERR_OK;
          attsdb_att_set_value(param->handle, sizeof(uint8_t) * char_code, (uint8_t *)&param->value[0]);	
					atts_write_rsp_send(proxr_env.con_info.conhdl, param->handle, status);
        }
    }

    return (KE_MSG_CONSUMED);
}

/**
 ****************************************************************************************
 * @brief Disconnection indication to proximity reporter.
 * Alert according to LLS alert level.
 * @param[in] msgid     Id of the message received.
 * @param[in] param     Pointer to the parameters of the message.
 * @param[in] dest_id   ID of the receiving task instance
 * @param[in] src_id    ID of the sending task instance.
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
static int gap_discon_cmp_evt_handler(ke_msg_id_t const msgid,
                                        struct gap_discon_cmp_evt const *param,
                                        ke_task_id_t const dest_id,
                                        ke_task_id_t const src_id)
{
    return (KE_MSG_CONSUMED);
}


/*
 * GLOBAL VARIABLE DEFINITIONS
 ****************************************************************************************
 */

/// Disabled State handler definition.
const struct ke_msg_handler proxr_disabled[] =
{
    {PROXR_CREATE_DB_REQ,   (ke_msg_func_t) proxr_create_db_req_handler},
};

/// Idle State handler definition.
const struct ke_msg_handler proxr_idle[] =
{
    {PROXR_ENABLE_REQ,      (ke_msg_func_t) proxr_enable_req_handler},
};

/// Connected State handler definition.
const struct ke_msg_handler proxr_connected[] =
{
    {GATT_WRITE_CMD_IND,    (ke_msg_func_t) gatt_write_cmd_ind_handler},
};

/// Default State handlers definition
const struct ke_msg_handler proxr_default_state[] =
{
    {GAP_DISCON_CMP_EVT,    (ke_msg_func_t) gap_discon_cmp_evt_handler},
};

/// Specifies the message handler structure for every input state.
const struct ke_state_handler proxr_state_handler[PROXR_STATE_MAX] =
{
    [PROXR_DISABLED]    = KE_STATE_HANDLER(proxr_disabled),
    [PROXR_IDLE]        = KE_STATE_HANDLER(proxr_idle),
    [PROXR_CONNECTED]   = KE_STATE_HANDLER(proxr_connected),
};

/// Specifies the message handlers that are common to all states.
const struct ke_state_handler proxr_default_handler = KE_STATE_HANDLER(proxr_default_state);

/// Defines the place holder for the states of all the task instances.
ke_state_t proxr_state[PROXR_IDX_MAX];


// Register PROXR task into kernel  
void task_proxr_desc_register(void)
{
    struct ke_task_desc task_proxr_desc;

    task_proxr_desc.state_handler = proxr_state_handler;
    task_proxr_desc.default_handler=&proxr_default_handler;
    task_proxr_desc.state = proxr_state;
    task_proxr_desc.state_max = PROXR_STATE_MAX;
    task_proxr_desc.idx_max = PROXR_IDX_MAX;

    task_desc_register(TASK_PROXR, task_proxr_desc);
}
#endif //BLE_PROX_REPORTER

/// @} PROXRTASK
