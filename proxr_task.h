/**
 ****************************************************************************************
 *
 * @file proxr_task.h
 *
 * @brief Header file - PROXRTASK.
 *
 * Copyright (C) RivieraWaves 2009-2012
 *
 * $Rev: 5767 $
 *
 ****************************************************************************************
 */

#ifndef PROXR_TASK_H_
#define PROXR_TASK_H_

/// @cond

/**
 ****************************************************************************************
 * @addtogroup PROXRTASK Proximity Reporter Task
 * @ingroup PROXR
 * @brief Proximity Reporter Task
 *
 * The PROXRTASK is responsible for handling the APi messages from the Application or internal
 * tasks.
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#if (BLE_PROX_REPORTER)

#include <stdint.h>
#include "ke_task.h"
#include "proxr.h"

/*
 * DEFINES
 ****************************************************************************************
 */

/// Maximum number of Proximity Reporter task instances
#define PROXR_IDX_MAX                 (1)

#define CHAR_1_LENGTH									16
#define CHAR_2_LENGTH									4
#define CHAR_3_LENGTH									1
#define CHAR_4_LENGTH									1
#define CHAR_5_LENGTH									16
#define CHAR_6_LENGTH									6
#define CHARS_LENGTH									48

#define BC_UUID "\xAD\xE8\xF3\xD4\xB8\x84\x94\xA0\xAA\xF5\xE2\x0F\x24\x15\x5A\x95"
#define MM_UUID "\xAD\xE8\xF3\xD4\xB8\x84\x94\xA0\xAA\xF5\xE2\x0F\x26\x15\x5A\x95"
#define BT_UUID "\xAD\xE8\xF3\xD4\xB8\x84\x94\xA0\xAA\xF5\xE2\x0F\x25\x15\x5A\x95"
#define LD_UUID "\xAD\xE8\xF3\xD4\xB8\x84\x94\xA0\xAA\xF5\xE2\x0F\x29\x15\x5A\x95"
#define NM_UUID "\xAD\xE8\xF3\xD4\xB8\x84\x94\xA0\xAA\xF5\xE2\x0F\x30\x15\x5A\x95"
#define OG_UUID "\xAD\xE8\xF3\xD4\xB8\x84\x94\xA0\xAA\xF5\xE2\x0F\x31\x15\x5A\x95"

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// Possible states of the PROXR task
enum
{
    /// Disabled State
    PROXR_DISABLED,
    /// Idle state
    PROXR_IDLE,
    /// Connected state
    PROXR_CONNECTED,

    /// Number of defined states.
    PROXR_STATE_MAX
};

/// PROXR_CREATE_DB_REQ - Bit values
enum
{
    PROXR_IAS_TXPS_NOT_SUP,
    PROXR_IAS_TXPS_SUP,
};

/// Messages for Proximity Reporter
enum
{
    /// Start the proximity reporter
    PROXR_ENABLE_REQ = KE_FIRST_MSG(TASK_PROXR),

    ///Add a LLS instance and optionally a TXPS instance into the database
    PROXR_CREATE_DB_REQ,

    /// Error Indication
    PROXR_ERROR_IND,
};


/*
 * API MESSAGES STRUCTURES
 ****************************************************************************************
 */
 
/// Parameters of the @ref PROXR_CREATE_DB_REQ message
struct proxr_create_db_req
{
    /// Indicate if TXPS is supported or not
    uint8_t features;
};

/// Parameters of the @ref PROXR_ENABLE_REQ message
struct proxr_enable_req
{
    /// Connection Handle
    uint16_t conhdl;

		/// TX Power level
    int8_t txp_lvl;
};


/// @endcond

/**
 ****************************************************************************************
 * @addtogroup APP_PROXR_TASK
 * @{
 ****************************************************************************************
 */

/// Parameters of the @ref PROXR_CREATE_DB_CFM message
struct proxr_create_db_cfm
{
    /// Status
    uint8_t status;
};

/// Parameters of the @ref PROXR_DISABLE_IND message
struct proxr_disable_ind
{
    /// Connection Handle
    uint16_t conhdl;
    /// LLS alert level to save in APP
    uint8_t  lls_alert_lvl;
};

/// Parameters of the @ref PROXR_ALERT_IND message
struct proxr_alert_ind
{
    /// Connection handle
    uint16_t conhdl;
    /// Alert level
    uint8_t alert_lvl;
    /// Char Code - Indicate if IAS or LLS
    uint8_t char_code;
};
/// @}APP_PROXR_TASK

/// @cond

/*
 * GLOBAL VARIABLES DECLARATIONS
 ****************************************************************************************
 */

extern ke_state_t proxr_state[PROXR_IDX_MAX];

extern void task_proxr_desc_register(void);

#endif //BLE_PROX_REPORTER

/// @} PROXRTASK
/// @endcond
#endif // PROXR_TASK_H_
