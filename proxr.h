/**
 ****************************************************************************************
 *
 * @file proxr.h
 *
 * @brief Header file - PROXR.
 *
 * Copyright (C) RivieraWaves 2009-2012
 *
 * $Rev: $
 *
 ****************************************************************************************
 */

#ifndef PROXR_H_
#define PROXR_H_

/**
 ****************************************************************************************
 * @addtogroup PROXR Proximity Reporter
 * @ingroup PROX
 * @brief Proximity Profile Reporter.
 *
 * The PROXR is responsible for providing proximity reporter functionalities to
 * upper layer module or application. The device using this profile takes the role
 * of a proximity reporter role.
 *
 * Proximity Reporter (PR): A PR (e.g. a watch, key fob, etc) is the term used by
 * this profile to describe a personal device that a user carries with them and that
 * has low power requirement (i.e. it is operating with a button cell), allowing another
 * device to monitor their connection. The device may have a simple user alert
 * functionality, for example a blinking LED or audible output.
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#if (BLE_PROX_REPORTER)

#include "ke_task.h"
#include "atts.h"
#include "prf_types.h"

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// LLS Handles offsets
enum
{
		IDX_SVC,
	
		IDX_BC_CHAR,
    IDX_BC_VAL,
		IDX_MM_CHAR,
    IDX_MM_VAL,
		IDX_BT_CHAR,
    IDX_BT_VAL,
		IDX_LD_CHAR,
    IDX_LD_VAL,
		IDX_NM_CHAR,
    IDX_NM_VAL,
		IDX_OG_CHAR,
    IDX_OG_VAL,
	
    IDX_NB
};

/*
 * STRUCTURES
 ****************************************************************************************
 */

/// Proximity reporter environment variable
struct proxr_env_tag
{
    /// Connection Information
    struct prf_con_info con_info;

    /// LLS Start Handle
    uint16_t ibc_shdl;
};

/*
 * PROXIMITY PROFILE ATTRIBUTES DECLARATION
 ****************************************************************************************
 */

extern uint8_t RG_UUID[ATT_UUID_128_LEN];
extern uint8_t DF_UUID[ATT_UUID_128_LEN];

extern const struct atts_desc_ext proxr_att_db[IDX_NB];

extern const struct atts_char128_desc proxr_bc_char;
extern const struct atts_char128_desc proxr_mm_char;
extern const struct atts_char128_desc proxr_bt_char;
extern const struct atts_char128_desc proxr_rs_char;
extern const struct atts_char128_desc proxr_nm_char;
extern const struct atts_char128_desc proxr_og_char;

/*
 * GLOBAL VARIABLE DECLARATIONS
 ****************************************************************************************
 */

extern struct proxr_env_tag proxr_env;

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Initialization of the PROXR module.
 * This function performs all the initializations of the PROXR module.
 ****************************************************************************************
 */
void proxr_init(void);

#endif //BLE_PROX_REPORTER

/// @} PROXR

#endif // PROXR_H_
