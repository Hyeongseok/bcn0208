/**
 ****************************************************************************************
 *
 * @file proxr.c
 *
 * @brief Proximity Reporter Implementation.
 *
 * Copyright (C) RivieraWaves 2009-2012
 *
 * $Rev: $
 *
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @addtogroup PROXR
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "app_config.h"

#if (BLE_PROX_REPORTER)

#include "proxr.h"
#include "proxr_task.h"
#include "atts_db.h"

/*
 * PROXIMITY PROFILE ATTRIBUTES DEFINITION
 ****************************************************************************************
 */

/// Full LLS Database Description - Used to add attributes into the database
const struct atts_desc_ext proxr_att_db[IDX_NB] =
{
    [IDX_SVC]                      =   {{ATT_UUID_16_LEN, (uint8_t *)"\x00\x28"}, PERM(RD, ENABLE), sizeof(RG_UUID),
                                         sizeof(RG_UUID), RG_UUID},

		[IDX_BC_CHAR]        =   {{ATT_UUID_16_LEN, (uint8_t *)"\x03\x28"}, PERM(RD, ENABLE), sizeof(proxr_bc_char),
                                         sizeof(proxr_bc_char), (uint8_t *)&proxr_bc_char},
		
    [IDX_BC_VAL]         =   {{ATT_UUID_128_LEN, (uint8_t *)BC_UUID}, PERM(RD, ENABLE) | PERM(WR, ENABLE), CHAR_1_LENGTH, 0, NULL},
    		
    [IDX_MM_CHAR]        =   {{ATT_UUID_16_LEN, (uint8_t *)"\x03\x28"}, PERM(RD, ENABLE), sizeof(proxr_mm_char),
                                         sizeof(proxr_mm_char), (uint8_t *)&proxr_mm_char},
		
    [IDX_MM_VAL]         =   {{ATT_UUID_128_LEN, (uint8_t *)MM_UUID}, PERM(RD, ENABLE) | PERM(WR, ENABLE), CHAR_2_LENGTH, 0, NULL},
		
		
		[IDX_BT_CHAR]        =   {{ATT_UUID_16_LEN, (uint8_t *)"\x03\x28"}, PERM(RD, ENABLE), sizeof(proxr_bt_char),
                                         sizeof(proxr_bt_char), (uint8_t *)&proxr_bt_char},
		
		
    [IDX_BT_VAL]         =   {{ATT_UUID_128_LEN, (uint8_t *)BT_UUID}, PERM(RD, ENABLE) | PERM(WR, ENABLE), CHAR_3_LENGTH, 0, NULL},
		
		[IDX_LD_CHAR]        =   {{ATT_UUID_16_LEN, (uint8_t *)"\x03\x28"}, PERM(RD, ENABLE), sizeof(proxr_rs_char),
                                         sizeof(proxr_rs_char), (uint8_t *)&proxr_rs_char},
		
    [IDX_LD_VAL]         =   {{ATT_UUID_128_LEN, (uint8_t *)LD_UUID}, PERM(RD, ENABLE) | PERM(WR, ENABLE), CHAR_4_LENGTH, 0, NULL},
		
		[IDX_NM_CHAR]        =   {{ATT_UUID_16_LEN, (uint8_t *)"\x03\x28"}, PERM(RD, ENABLE), sizeof(proxr_nm_char),
                                         sizeof(proxr_nm_char), (uint8_t *)&proxr_nm_char},
		
    [IDX_NM_VAL]         =   {{ATT_UUID_128_LEN, (uint8_t *)NM_UUID}, PERM(RD, ENABLE) | PERM(WR, ENABLE), CHAR_5_LENGTH, 0, NULL},
		
		[IDX_OG_CHAR]        =   {{ATT_UUID_16_LEN, (uint8_t *)"\x03\x28"}, PERM(RD, ENABLE), sizeof(proxr_og_char),
                                         sizeof(proxr_og_char), (uint8_t *)&proxr_og_char},
		
    [IDX_OG_VAL]         =   {{ATT_UUID_128_LEN, (uint8_t *)OG_UUID}, PERM(RD, ENABLE) | PERM(WR, ENABLE), CHAR_6_LENGTH, 0, NULL},
};

/*
 *  PROXIMITY PROFILE ATTRIBUTES VALUES DEFINTION
 ****************************************************************************************
 */

/// Link Loss Service - Alert Level Characteristic

const struct atts_char128_desc proxr_bc_char = ATTS_CHAR128(ATT_CHAR_PROP_WR | ATT_CHAR_PROP_RD, 0, BC_UUID);

const struct atts_char128_desc proxr_mm_char = ATTS_CHAR128(ATT_CHAR_PROP_RD, 0, MM_UUID);

const struct atts_char128_desc proxr_bt_char = ATTS_CHAR128(ATT_CHAR_PROP_RD, 0, BT_UUID);

const struct atts_char128_desc proxr_rs_char = ATTS_CHAR128(ATT_CHAR_PROP_WR | ATT_CHAR_PROP_RD, 0, LD_UUID);

const struct atts_char128_desc proxr_nm_char = ATTS_CHAR128(ATT_CHAR_PROP_WR | ATT_CHAR_PROP_RD, 0, NM_UUID);

const struct atts_char128_desc proxr_og_char = ATTS_CHAR128(ATT_CHAR_PROP_WR | ATT_CHAR_PROP_RD, 0, OG_UUID);

/*
 * GLOBAL VARIABLE DEFINITIONS
 ****************************************************************************************
 */

struct proxr_env_tag proxr_env;

uint8_t RG_UUID[ATT_UUID_128_LEN] = {0xAD, 0xE8, 0xF3, 0xD4, 0xB8, 0x84, 0x94, 0xA0, 0xAA, 0xF5, 0xE2, 0x0F, 0x23, 0x15, 0x5A, 0x95};

/*
 * FUNCTION DEFINITIONS
 ****************************************************************************************
 */

void proxr_init(void)
{
    // Reset Environment
    memset(&proxr_env, 0, sizeof(proxr_env));

    // Register PROXR task into kernel    
    task_proxr_desc_register();
    
    ke_state_set(TASK_PROXR, PROXR_DISABLED);
}

#endif //BLE_PROX_REPORTER

/// @} PROXR
