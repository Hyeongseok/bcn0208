/**
 ****************************************************************************************
 *
 * @file usr_design.c
 *
 * @brief Product related design.
 *
 * Copyright(C) 2015 NXP Semiconductors N.V.
 * All rights reserved.
 *
 * $Rev: 1.0 $
 *
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @addtogroup  USR
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include <stdint.h>
#include "app_env.h"
#include "led.h"
#include "adc.h"
#include "analog.h"
#include "uart.h"
#include "lib.h"
#include "usr_design.h"
#include "gpio.h"
#include "pwm.h"
#include "button.h"
#include "sleep.h"
#include "serialflash.h"
#include "i2c.h"
#include "proxr_task.h"
#include "qnrf.h"

/*
 * GLOBAL VARIABLE DEFINITIONS
 ****************************************************************************************
 */

struct usr_env_tag usr_env = {LED_ON_DUR_IDLE, LED_OFF_DUR_IDLE};
struct bd_addr addr;
// Update connection parameters here
struct gap_conn_param_update conn_par = {
        /// Connection interval minimum
        GAP_PPCP_CONN_INTV_MIN,
        /// Connection interval maximum
        GAP_PPCP_CONN_INTV_MAX,
        /// Latency
        GAP_PPCP_SLAVE_LATENCY,
        /// Supervision timeout, Time = N * 10 msec
        GAP_PPCP_STO_MULT
};

uint8_t buffer[CHARS_LENGTH], cfg_buf[256], rx_buf[72];
uint8_t btn_flag = 0, rx_flag = 0, register_flag = 0, i2c_counter = 0, beep_counter = 0, acc_flag = 0;
short data[4];
uint32_t reg_mask0 = P16_MASK_PIN_CTRL | P17_MASK_PIN_CTRL, reg_mask1 = P26_MASK_PIN_CTRL;

/*
 * FUNCTION DEFINITIONS
 ****************************************************************************************
 */

// The fully charged voltage and fully discharged voltage value, unit: mv
#define BASS_FULLY_CHARGED_VOLTAGE        3000
#define BASS_FULLY_DISCHARGED_VOLTAGE     2300

// the event id of button1
#define EVENT_BUTTON1_PRESS_ID            0

// the event id of adc sample complete
#define EVENT_ADC_SAMPLE_CMP_ID           1

/**
 ****************************************************************************************
 * @brief   Led1 for BLE status
 ****************************************************************************************
 */
static void usr_led1_set(uint16_t timer_on, uint16_t timer_off)
{	
    usr_env.led1_on_dur = timer_on;
    usr_env.led1_off_dur = timer_off;
	
    if (timer_on == 0 || timer_off == 0)
    {				
        if (timer_on == 0)
        {
            led_set(1, LED_OFF);
        }
        if (timer_off == 0)
        {
            led_set(1, LED_ON);
        }
        ke_timer_clear(APP_SYS_LED_1_TIMER, TASK_APP);
    }
    else
    {
        led_set(1, LED_OFF);
        ke_timer_set(APP_SYS_LED_1_TIMER, TASK_APP, timer_off);
    }
}

void blink_led(uint16_t time){	
			usr_led1_set(time/2, time);
			ke_timer_set(APP_SYS_LED_2_TIMER, TASK_APP, 100);
}

/**
 ****************************************************************************************
 * @brief   Led 1 flash process
 ****************************************************************************************
 */
static void usr_led1_process(void)
{		
    if(led_get(1) == LED_ON)
    {
        led_set(1, LED_OFF);
        ke_timer_set(APP_SYS_LED_1_TIMER, TASK_APP, usr_env.led1_off_dur);
    }
    else
    {
        led_set(1, LED_ON);
        ke_timer_set(APP_SYS_LED_1_TIMER, TASK_APP, usr_env.led1_on_dur);
    }
}

/**
 ****************************************************************************************
 * @brief ADC sample complete handler
 ****************************************************************************************
 */
void app_event_adc_sample_cmp_handler(void)
{
    int     bas_average_adc_value = 0;
    int     bas_voltage;
    uint8_t bas_percentage;
    int     i;

    ke_evt_clear(1UL << EVENT_ADC_SAMPLE_CMP_ID);

    // Close ADC and battery monitor
    battery_monitor_enable(MASK_DISABLE);
    adc_clock_off();
    adc_power_off();

    // Calculate average value
    for(i = 0; i < BASS_SAMPLE_NUMBER; ++i)
        bas_average_adc_value += usr_env.bas_reg_buf[i];
    bas_average_adc_value /= BASS_SAMPLE_NUMBER;

    // When enable ADC decimation, the adc value should div 4
    bas_average_adc_value /= 4;
    
    // bas voltage is 4 times
    bas_voltage = 4 * ADC_RESULT_mV(bas_average_adc_value);

    // Calculate the percentage of remaining battery
    if(bas_voltage <= BASS_FULLY_DISCHARGED_VOLTAGE)
    {
        bas_percentage = 0;
    }
    else if(bas_voltage >= BASS_FULLY_CHARGED_VOLTAGE)
    {
        bas_percentage = 100;
    }
    else
    {
        bas_percentage = (uint8_t)((bas_voltage - BASS_FULLY_DISCHARGED_VOLTAGE) * 100 / (BASS_FULLY_CHARGED_VOLTAGE - BASS_FULLY_DISCHARGED_VOLTAGE));
    }
		
		#ifdef CFG_DBG_PRINT
			QPRINTF("battery:%dmv(%d%%) adc:%d\r\n", bas_voltage, bas_percentage, bas_average_adc_value);
		#endif
		
		buffer[CHAR_1_LENGTH + CHAR_2_LENGTH] = bas_percentage;
}

/**
 ****************************************************************************************
 * @brief callback of adc sample complete.
 ****************************************************************************************
 */
void adc_sample_complete_callback(void)
{
    ke_evt_set(1UL << EVENT_ADC_SAMPLE_CMP_ID);
}

/**
 ****************************************************************************************
 * @brief   Application task message handler
 ****************************************************************************************
 */
void app_task_msg_hdl(ke_msg_id_t const msgid, void const *param)
{			
    switch(msgid)
    {
				case GAP_SET_MODE_REQ_CMP_EVT:	
            if(APP_IDLE == ke_state_get(TASK_APP))
            {			
							ke_timer_set(APP_ADV_INTV_UPDATE_TIMER, TASK_APP, 0);
							ke_timer_set(APP_I2C_WRITE_TIMER, TASK_APP, 0);
            }
            break;

        case GAP_DISCON_CMP_EVT:			
						app_gap_adv_start_req(GAP_UND_CONNECTABLE, app_env.adv_data, app_set_adv_data(1),  app_env.scanrsp_data, app_set_scan_rsp_data(1), GAP_ADV_FAST_INTV1, GAP_ADV_FAST_INTV1);

						syscon_SetPMCR0WithMask(QN_SYSCON, reg_mask0, P15_PWM1_PIN_CTRL | P16_PWM0_PIN_CTRL);
						syscon_SetPMCR1WithMask(QN_SYSCON, reg_mask1, P26_GPIO_22_PIN_CTRL);
				
						pwm_config(PWM_CH1, PWM_PSCAL_DIV, PWM_COUNT_MS(20, PWM_PSCAL_DIV), PWM_COUNT_US(1000, PWM_PSCAL_DIV));
						pwm_config(PWM_CH0, PWM_PSCAL_DIV, PWM_COUNT_MS(20, PWM_PSCAL_DIV), PWM_COUNT_US(2000, PWM_PSCAL_DIV));
				
						delay(160000);
				
						syscon_SetPMCR0WithMask(QN_SYSCON, reg_mask0, P15_GPIO_13_PIN_CTRL | P16_GPIO_14_PIN_CTRL);
							
						power_on_flash();
						sector_erase_flash(0x00, 1);
						memcpy(&cfg_buf[208], buffer, CHARS_LENGTH);
						write_flash(0x00, (uint32_t *)cfg_buf, 256);
						power_off_flash();
            break;

        case GAP_LE_CREATE_CONN_REQ_CMP_EVT:			
            if(((struct gap_le_create_conn_req_cmp_evt *)param)->conn_info.status == CO_ERROR_NO_ERROR)
            {
								if(register_flag == 1){ // device is already registered
									attsdb_att_set_permission(proxr_env.ibc_shdl + IDX_NM_VAL, PERM(WR,  DISABLE));
									attsdb_att_set_permission(proxr_env.ibc_shdl + IDX_OG_VAL, PERM(WR,  DISABLE));
								}
								app_bass_batt_level_timer_handler(APP_BASS_BATT_LEVEL_TIMER, NULL, TASK_APP, TASK_APP);
								app_gap_param_update_req(((struct gap_le_create_conn_req_cmp_evt *)param)->conn_info.conhdl, &conn_par);
            }	
            break;				
						        
        default:
            break;
    }
}

/**
 ****************************************************************************************
 * @brief Handles LED status timer.
 *
 * @param[in] msgid      APP_SYS_UART_DATA_IND
 * @param[in] param      Pointer to struct app_uart_data_ind
 * @param[in] dest_id    TASK_APP
 * @param[in] src_id     TASK_APP
 *
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
int app_led_timer_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
	uint8_t reset[48] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		
    if(msgid == APP_SYS_LED_1_TIMER)
    {
        usr_led1_process();
    }
		if(msgid == APP_SYS_LED_2_TIMER)
    {
				if(btn_flag == 1){
					// Power on inside serial flash
					power_on_flash();
					sector_erase_flash(0x00, 1);
					memcpy(&cfg_buf[208], reset, CHARS_LENGTH);
					write_flash(0x00, (uint32_t *)cfg_buf, 256);
					power_off_flash();
								
					NVIC_SystemReset();
				}
    }

    return (KE_MSG_CONSUMED);
}

void uart_rx_cb(void)
{
		char *split_char;
	
		if(rx_buf[0]==0x0A){
			rx_flag = 1;
			
			if((split_char = strchr((const char *)rx_buf, '*')) != NULL){
				*split_char = '\0';
			}
		
			if(register_flag == 1)
				uart_printf(QN_UART0, rx_buf);
		}
}

/**
 ****************************************************************************************
 * @brief Handles advertising mode timer event.
 *
 * @param[in] msgid     APP_ADV_INTV_UPDATE_TIMER
 * @param[in] param     None
 * @param[in] dest_id   TASK_APP
 * @param[in] src_id    TASK_APP
 *
 * @return If the message was consumed or not.
 * @description
 *
 * This handler is used to inform the application that first phase of adversting mode is timeout.
 ****************************************************************************************
 */

int app_i2c_write_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id){
		uint8_t sonar_sensor_data[8];
		uint32_t acc_sensor_data = 0;
	
		switch(i2c_counter++ % 4){
			case 0:
				I2C_BYTE_WRITE(0x70, 0x00, 0x51);
				delay(50000);
			
				sonar_sensor_data[0] = I2C_BYTE_READ(0x70, 0x02);
				sonar_sensor_data[1] = I2C_BYTE_READ(0x70, 0x03);
			
				if(sonar_sensor_data[0] != 0){
					data[0] = 0xff;
				}
				else{
					data[0] =  sonar_sensor_data[1];
				}
				break;
			
			case 1:
				I2C_BYTE_WRITE(0x71, 0x00, 0x51);
				delay(50000);
			
				sonar_sensor_data[2] = I2C_BYTE_READ(0x71, 0x02);
				sonar_sensor_data[3] = I2C_BYTE_READ(0x71, 0x03);
			
				if(sonar_sensor_data[2] != 0){
					data[1] = 0xff;
				}
				else{
					data[1] =  sonar_sensor_data[3];
				}
				break;
			
			case 2:
				I2C_BYTE_WRITE(0x72, 0x00, 0x51);
				delay(50000);

				sonar_sensor_data[4] = I2C_BYTE_READ(0x72, 0x02);
				sonar_sensor_data[5] = I2C_BYTE_READ(0x72, 0x03);
			
				if(sonar_sensor_data[4] != 0){
					data[2] = 0xff;
				}
				else{
					data[2] =  sonar_sensor_data[5];
				}
				break;
			
			case 3:
				I2C_BYTE_WRITE(0x73, 0x00, 0x51);
				delay(50000);
				sonar_sensor_data[6] = I2C_BYTE_READ(0x73, 0x02);
				sonar_sensor_data[7] = I2C_BYTE_READ(0x73, 0x03);
			
				if(sonar_sensor_data[6] != 0){
					data[3] = 0xff;
				}
				else{
					data[3] =  sonar_sensor_data[7];
				}
				break;
			
			default:
				break;
		}
		
		if((data[0] > 0 && data[0] < 25) || (data[1] > 0 && data[1] < 25) || (data[2] > 0 && data[2] < 25) || (data[3] > 0 && data[3] < 25)){
				pwm_config(PWM_CH1, PWM_PSCAL_DIV, PWM_COUNT_MS(1, PWM_PSCAL_DIV), PWM_COUNT_US(200, PWM_PSCAL_DIV));
			
				if(beep_counter++ %2 == 0){
					syscon_SetPMCR1WithMask(QN_SYSCON, reg_mask1, P26_PWM1_PIN_CTRL);
				}
				else{
					syscon_SetPMCR1WithMask(QN_SYSCON, reg_mask1, P26_GPIO_22_PIN_CTRL);
				}
		}
		else{
			syscon_SetPMCR1WithMask(QN_SYSCON, reg_mask1, P26_GPIO_22_PIN_CTRL);
		}
		
		acc_sensor_data += I2C_BYTE_READ(0x68, 0x3B) << 8;
		acc_sensor_data += I2C_BYTE_READ(0x68, 0x3C);
		acc_sensor_data += I2C_BYTE_READ(0x68, 0x3D) << 8;
		acc_sensor_data += I2C_BYTE_READ(0x68, 0x3E);
		acc_sensor_data += I2C_BYTE_READ(0x68, 0x3F) << 8;
		acc_sensor_data += I2C_BYTE_READ(0x68, 0x40);
		
		if(acc_sensor_data < 40000 || acc_sensor_data > 80000){
			acc_flag = 1;
		}
		else{
			acc_flag = 0;
		}
		
		ke_timer_set(APP_I2C_WRITE_TIMER, TASK_APP, 5);
		return (KE_MSG_CONSUMED);
}

int app_gap_adv_intv_update_timer_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
			uint8_t str_buf[100], id_buf[17], org_buf[7];
			
			buffer[CHAR_1_LENGTH] = data[0];
			buffer[CHAR_1_LENGTH + 1] = data[1];							
			buffer[CHAR_1_LENGTH + 2] = data[2];							
			buffer[CHAR_1_LENGTH + 3] = data[3];
	
			if(register_flag == 1){
				memcpy((char *) id_buf, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH + CHAR_4_LENGTH], CHAR_5_LENGTH);
				memcpy((char *) org_buf, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH + CHAR_3_LENGTH + CHAR_4_LENGTH + CHAR_5_LENGTH], CHAR_6_LENGTH);
				id_buf[CHAR_5_LENGTH] = org_buf[CHAR_6_LENGTH] = '\0';
				sprintf((char *) str_buf,"\n{\"id\":\"%s\",\"org\":\"%s\",\"s1\":\"%d\",\"s2\":\"%d\",\"s3\":\"%d\",\"s4\":\"%d\",\"s5\":\"%d\"}", id_buf, org_buf, data[0], data[1], data[2], data[3], acc_flag);
				uart_printf(QN_UART0, str_buf);
				
				uart_read(QN_UART0, rx_buf, 72, uart_rx_cb);
			}
							
			app_bass_batt_level_timer_handler(APP_BASS_BATT_LEVEL_TIMER, NULL, TASK_APP, TASK_APP);
							
			attsdb_att_set_value(proxr_env.ibc_shdl + IDX_MM_VAL, sizeof(uint8_t) * CHAR_2_LENGTH, &buffer[CHAR_1_LENGTH]);
			attsdb_att_set_value(proxr_env.ibc_shdl + IDX_BT_VAL, sizeof(uint8_t) * CHAR_3_LENGTH, &buffer[CHAR_1_LENGTH + CHAR_2_LENGTH]);
			
			ke_timer_set(APP_ADV_INTV_UPDATE_TIMER, TASK_APP, 100); // refresh

			return (KE_MSG_CONSUMED);
}

/**
 ****************************************************************************************
 * @brief Handles the battery level timer.
 *
 * @param[in] msgid     Id of the message received
 * @param[in] param     None
 * @param[in] dest_id   TASK_APP
 * @param[in] src_id    TASK_APP
 *
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
int app_bass_batt_level_timer_handler(ke_msg_id_t const msgid, void *param, ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    // Battery monitor enable
    battery_monitor_enable(MASK_ENABLE);

    // ADC initialize
    adc_init(ADC_SINGLE_WITH_BUF_DRV, ADC_CLK_1000000, ADC_INT_REF, ADC_12BIT);

    // Enable ADC decimation
    adc_decimation_enable(DECI_RATE_64, MASK_ENABLE);

    // Read voltage. use interrupt
    adc_read_configuration read_cfg;
    read_cfg.trig_src = ADC_TRIG_SOFT;
    read_cfg.mode = CONTINUE_MOD;
    read_cfg.start_ch = BATT;
    read_cfg.end_ch = BATT;
    adc_read(&read_cfg, usr_env.bas_reg_buf, BASS_SAMPLE_NUMBER, adc_sample_complete_callback);

    return (KE_MSG_CONSUMED);
}

/**
 ****************************************************************************************
 * @brief   Restore peripheral setting after wakeup
 ****************************************************************************************
 */
void usr_sleep_restore(void)
{
    uart_init(QN_DEBUG_UART, USARTx_CLK(0), UART_9600);
    uart_tx_enable(QN_DEBUG_UART, MASK_ENABLE);
    uart_rx_enable(QN_DEBUG_UART, MASK_ENABLE);
}

/**
 ****************************************************************************************
 * @brief Handles button press after cancel the jitter.
 *
 * @param[in] msgid     Id of the message received
 * @param[in] param     None
 * @param[in] dest_id   TASK_APP
 * @param[in] src_id    TASK_APP
 *
 * @return If the message was consumed or not.
 ****************************************************************************************
 */

int app_button_timer_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id)
{	
		int i;
			
    switch(msgid)
    {
        case APP_SYS_BUTTON_1_TIMER:
							for(i = 0; gpio_read_pin(BUTTON1_PIN) == GPIO_LOW; i++)
							{
								if(i > 600000)
									{
											btn_flag = 1;
											break;
									}
							}

							if(btn_flag == 1){
								blink_led(LED_DUR_RST);
							}
						break;

        default:
            ASSERT_ERR(0);
            break;
    }

    return (KE_MSG_CONSUMED);
	}

/**
 ****************************************************************************************
 * @brief Handles button press before key debounce.
 * @return If the message was consumed or not.
 ****************************************************************************************
 */
void app_event_button1_press_handler(void)
{
#if ((QN_DEEP_SLEEP_EN) && (!QN_32K_RCO))
    if (sleep_env.deep_sleep) 
    {
        sleep_env.deep_sleep = false;
        // start 32k xtal wakeup timer
        wakeup_32k_xtal_start_timer();
    }
#endif

    // delay 20ms to debounce
    ke_timer_set(APP_SYS_BUTTON_1_TIMER, TASK_APP, 2);
    ke_evt_clear(1UL << EVENT_BUTTON1_PRESS_ID);
}

/**
 ****************************************************************************************
 * @brief   Button 1 click callback
 * @description
 *  Button 1 is used to enter adv mode.
 ****************************************************************************************
 */
void usr_button1_cb(void)
{
    // If BLE is in the sleep mode, wakeup it.
    if(ble_ext_wakeup_allow())
    {
#if ((QN_DEEP_SLEEP_EN) && (!QN_32K_RCO))
        if (sleep_env.deep_sleep)
        {
            wakeup_32k_xtal_switch_clk();
        }
#endif
    
        sw_wakeup_ble_hw();

 #if (QN_DEEP_SLEEP_EN)
         // prevent deep sleep
         if(sleep_get_pm() == PM_DEEP_SLEEP)
         {
             sleep_set_pm(PM_SLEEP);
         }
 #endif
    }

    // key debounce:
    // We can set a soft timer to debounce.
    // After wakeup BLE, the timer is not calibrated immediately and it is not precise.
    // So We set a event, in the event handle, set the soft timer.
    ke_evt_set(1UL << EVENT_BUTTON1_PRESS_ID);
}

/**
 ****************************************************************************************
 * @brief   All GPIO interrupt callback
 ****************************************************************************************
 */
void gpio_interrupt_callback(enum gpio_pin pin)
{
    switch(pin)
    {
        case BUTTON1_PIN:
            //Button 1 is used to enter adv mode.
            usr_button1_cb();
            break;

#if (defined(QN_TEST_CTRL_PIN))
        case QN_TEST_CTRL_PIN:
            //When test controll pin is changed to low level, this function will reboot system.
            gpio_disable_interrupt(QN_TEST_CTRL_PIN);
            syscon_SetCRSS(QN_SYSCON, SYSCON_MASK_REBOOT_SYS);
            break;
#endif

        default:
            break;
    }
}

/**
 ****************************************************************************************
 * @brief   User initialize
 ****************************************************************************************
 */
void usr_init(void)
{
		char compare[17] = {0xff, 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff, '\0'}, substr[17];
		memcpy(substr, buffer, 16);
		substr[16] = '\0';
								
		if(strcmp(compare, substr) != 0){ // device is already registered
			register_flag = 1;
		}
		
		if(KE_EVENT_OK != ke_evt_callback_set(EVENT_BUTTON1_PRESS_ID, app_event_button1_press_handler))
    {
        ASSERT_ERR(0);
    }
		
		// Register button ADC sample event callback
    if(KE_EVENT_OK != ke_evt_callback_set(EVENT_ADC_SAMPLE_CMP_ID, app_event_adc_sample_cmp_handler))
    {
        ASSERT_ERR(0);
    }
		
		rf_tx_power_level_set(TX_GAIN_LEVEL_MAX);
}

/// @} USR

