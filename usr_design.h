/**
 ****************************************************************************************
 *
 * @file usr_design.h
 *
 * @brief Product related design header file.
 *
 * Copyright(C) 2015 NXP Semiconductors N.V.
 * All rights reserved.
 *
 * $Rev: 1.0 $
 *
 ****************************************************************************************
 */

#ifndef USR_DESIGN_H_
#define USR_DESIGN_H_

#define QN_DEMO_AUTO								1

#define LED_DUR_ADV        (uint16_t)((GAP_ADV_SLOW_MIN_INTV*0.625)/10) / 1.5
#define LED_DUR_BLK       (uint16_t)((GAP_ADV_SLOW_MIN_INTV*0.625)/20) / 1.5
#define LED_DUR_RST       (uint16_t)((GAP_ADV_SLOW_MIN_INTV*0.625)/100) / 1.5
#define LED_ON_DUR_IDLE            0
#define LED_OFF_DUR_IDLE           0xffff

// The number of sample to battery voltage 
#define BASS_SAMPLE_NUMBER                16

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "app_env.h"
#include "gpio.h"

/*
 * STRUCTURE DEFINITIONS
 ****************************************************************************************
 */

struct usr_env_tag
{
    uint16_t    led1_on_dur;
    uint16_t    led1_off_dur;
	
		int16_t     bas_reg_buf[BASS_SAMPLE_NUMBER];
};

extern struct usr_env_tag usr_env;
extern uint8_t buffer[CHARS_LENGTH], cfg_buf[256], register_flag;

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */
extern int app_i2c_write_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id);
extern void app_task_msg_hdl(ke_msg_id_t const msgid, void const *param);
extern int app_led_timer_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id);
extern int app_gap_adv_intv_update_timer_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id);
extern int app_bass_batt_level_timer_handler(ke_msg_id_t const msgid, void *param, ke_task_id_t const dest_id, ke_task_id_t const src_id);
extern int app_proxr_alert_to_handler(ke_msg_id_t const msgid, void *param, ke_task_id_t const dest_id, ke_task_id_t const src_id);
extern void usr_sleep_restore(void);
extern void usr_button1_cb(void);
extern int app_button_timer_handler(ke_msg_id_t const msgid, void const *param, ke_task_id_t const dest_id, ke_task_id_t const src_id);
extern void usr_init(void);
extern void gpio_interrupt_callback(enum gpio_pin pin);
extern void blink_led(uint16_t time);

#endif
